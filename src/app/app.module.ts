import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProdottoInsertComponent } from './prodotto-insert/prodotto-insert.component';

import { HttpClientModule } from '@angular/common/http';
import { ProdottoListComponent } from './prodotto-list/prodotto-list.component';
import { ProdottoDetailsComponent } from './prodotto-details/prodotto-details.component';

@NgModule({
  declarations: [
    AppComponent,
    ProdottoInsertComponent,
    ProdottoListComponent,
    ProdottoDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule      //Mi serve per effettuare chiamate all'esterno.
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
