import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Prodotto } from '../prodotto';
import { ProdottoService } from '../prodotto.service';

@Component({
  selector: 'app-prodotto-insert',
  templateUrl: './prodotto-insert.component.html',
  styleUrls: ['./prodotto-insert.component.css']
})
export class ProdottoInsertComponent implements OnInit {

  varNome: any;
  varCodice: any;
  varPrezzo: any;

  showTuttoOk: Boolean = false;

  constructor(
    private service: ProdottoService,
    private router: Router
    ) { }

  ngOnInit(): void {
  }

  inserisciProdotto(){
    
    var prodotto = new Prodotto();
    prodotto.codice = this.varCodice;
    prodotto.nome = this.varNome;
    prodotto.prezzo = this.varPrezzo;

    this.service.addProdotto(prodotto).subscribe(
      (risultato) => {                        //Equivalente della Success di AJAX
        if(risultato){
          this.showTuttoOk = true;

          setTimeout(() => {
            this.router.navigateByUrl("prodotto/lista");
          }, 2000);
          
        }
        else{
          alert("Errore di esecuzione dell'operazione")
        }

      },
      (errore) => {                           //Equivalente della error di AJAX
        alert("errore di esecuzione")
        console.log(errore)
      }
    )

  }
}
