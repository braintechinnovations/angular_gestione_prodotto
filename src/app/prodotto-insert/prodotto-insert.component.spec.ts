import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdottoInsertComponent } from './prodotto-insert.component';

describe('ProdottoInsertComponent', () => {
  let component: ProdottoInsertComponent;
  let fixture: ComponentFixture<ProdottoInsertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProdottoInsertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdottoInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
