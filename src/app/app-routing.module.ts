import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProdottoDetailsComponent } from './prodotto-details/prodotto-details.component';
import { ProdottoInsertComponent } from './prodotto-insert/prodotto-insert.component';
import { ProdottoListComponent } from './prodotto-list/prodotto-list.component';

const routes: Routes = [
  {
    path: "prodotto/inserisci",
    component: ProdottoInsertComponent
  },
  {
    path: "prodotto/lista",
    component: ProdottoListComponent
  },
  {
    path: "prodotto/dettaglio/:prodid",
    component: ProdottoDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
