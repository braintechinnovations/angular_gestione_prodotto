import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Prodotto } from './prodotto';

const endpoint = "http://localhost:8085/prodotto"

@Injectable({
  providedIn: 'root'
})
export class ProdottoService {

  constructor(private http: HttpClient) { 
  }

  addProdotto(objProd: Prodotto){
    var header_custom = new HttpHeaders();
    header_custom = header_custom.set('Content-Type', 'application/json');

    //http://localhost:8085/prodotto/inserisci
    return this.http.post<Prodotto>(endpoint + "/inserisci", JSON.stringify(objProd), {headers: header_custom})
  }

  findAllProdotto(){
    return this.http.get(endpoint + "/")
  }

  findByIdProdotto(varId: number){
    return this.http.get<Prodotto>(endpoint + "/" + varId);
  }
  
  deleteProdotto(varId: number){
    return this.http.delete<Boolean>(endpoint + "/" + varId);
  }

  updateProdotto(objProd: Prodotto){
    var header_custom = new HttpHeaders();
    header_custom = header_custom.set('Content-Type', 'application/json');

    return this.http.put<Boolean>(endpoint + "/modifica", JSON.stringify(objProd), {headers: header_custom});
  }


}
