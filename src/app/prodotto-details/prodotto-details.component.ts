import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Prodotto } from '../prodotto';
import { ProdottoService } from '../prodotto.service';

@Component({
  selector: 'app-prodotto-details',
  templateUrl: './prodotto-details.component.html',
  styleUrls: ['./prodotto-details.component.css']
})
export class ProdottoDetailsComponent implements OnInit {

  varId : any;
  varNome : any;
  varCodice : any;
  varPrezzo : any;

  showTuttoOk: Boolean = false;

  constructor(
    private rottaAttiva: ActivatedRoute,
    private service: ProdottoService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.rottaAttiva.params.subscribe(
      (parametri) => {
        // console.log(parametri.prodid);

        this.service.findByIdProdotto(parametri.prodid).subscribe(
          (risultato) => {
            // console.log(risultato)
            this.varId = risultato.id;
            this.varNome = risultato.nome;
            this.varCodice = risultato.codice;
            this.varPrezzo = risultato.prezzo;
          },
          (errore) => {
            alert("Ooooops!");
            console.log(errore);
          }
        )
      }
    )
  }

  redirectSuLista(){
    setTimeout(() => {
            this.router.navigateByUrl("prodotto/lista");
          }, 2000);
  }

  eliminaProdotto(){
    // console.log(this.varId)
    this.service.deleteProdotto(this.varId).subscribe(
      (risultato) => {
        // if(risultato){       //Simile al comportamento di sotto
        //   alert("Elminazione effettuata con successo!")
        //   return;
        // }
        // alert("Ooooops, errore di eliminazione!");

        if(risultato){
          // alert("Elminazione effettuata con successo!");   //DEPRECATO
          this.showTuttoOk = true;
          this.redirectSuLista();          
        }
        else{
          alert("Ooooops, errore di eliminazione!");
        }
        
      },
      (errore) => {
        alert("Ooooops, errore del server!");
        console.log(errore);
      }
    )
  }

  modificaProdotto(){

    var prodTemp = new Prodotto();
    prodTemp.id = this.varId;       //L'unico campo che non può essere modificato
    prodTemp.codice = this.varCodice;
    prodTemp.nome = this.varNome;
    prodTemp.prezzo = this.varPrezzo;

    this.service.updateProdotto(prodTemp).subscribe(
      (risultato) => {
        if(risultato){
          this.showTuttoOk = true;
          this.redirectSuLista();
        }
        else{
          alert("Errore di esecuzione");
        }
      },
      (errore) => {
        alert("Errore di esecuzione")
        console.log(errore)
      }
    )

  }

}
