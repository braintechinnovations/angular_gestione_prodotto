import { Component, OnInit } from '@angular/core';
import { ProdottoService } from '../prodotto.service';

@Component({
  selector: 'app-prodotto-list',
  templateUrl: './prodotto-list.component.html',
  styleUrls: ['./prodotto-list.component.css']
})
export class ProdottoListComponent implements OnInit {

  elenco: any = [];

  constructor(private service: ProdottoService) { }

  ngOnInit(): void {

    this.service.findAllProdotto().subscribe(
      (risultato) => {              //La arrow function a differenza della funzione anonima classica, ci permette di accedere agli attributi dell'oggetto.
        this.elenco = risultato;
        console.log(this.elenco);
      },
      (errore) =>{
        alert("errore di esecuzione")
        console.log(errore)
      }
    )

  }

}
