export class Prodotto {
    id: number | undefined;
    codice: string | undefined;
    // nome: string | undefined = "N.D.";           //Inserimento valore di default
    nome: string | undefined;
    prezzo: number | undefined;
}
